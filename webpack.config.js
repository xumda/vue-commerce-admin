'use strict'
const path = require('path')

function resolve (dir) {
  return path.join(__dirname, '.', dir)
}

module.exports = {
  context: path.resolve(__dirname, './'),
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      '@': resolve('src'),
      '@img': resolve('src/assets/img'),
      '@pages': resolve('src/views/pages'),
      '@router': resolve('src/router'),
      '@store': resolve('src/store'),
      'utils': resolve('src/utils')
    }
  }
}

export const formatTime = (time) => {
  if (!time || typeof time !== 'object') {
    return time
  }
  for (let i = 1; i < time.length; i++) {
    time[i] = time[i] < 10 ? '0' + time[i] : '' + time[i]
  }
  return time[0] + '-' + time[1] + '-' + time[2] + ' ' + time[3] + ':' + time[4] + ':' + time[5]
}

export const formatTime2 = (time) => {
  if (!time || typeof time !== 'object') {
    return time
  }
  for (let i = 1; i < time.length; i++) {
    time[i] = time[i] < 10 ? '0' + time[i] : '' + time[i]
  }
  return time[0] + '年' + time[1] + '月' + time[2] + '日 ' + time[3] + '时' + time[4] + '分' + time[5] + '秒'
}

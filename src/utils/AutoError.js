/**
 * 自动处理错误，一个函数数组
 * 处理错误时顺序回调，返回true截断
 * @type {(function(Object): Boolean)[]}
 */
import { ResultCode } from '@/constants/constants'
import router from '@/router/index'
import store from '@/store/index'
import constants from '@/store/user/constants'

const AutoError = [
  // 未登录
  data => {
    if (data.code === ResultCode.CODE_NOT_LOGIN) {
      if (store.getters['user/isLogin']) {
        // 使用token二次登陆
        store.commit('user/' + constants.MUTATIONS_SET_LOGIN_USER_INFO, null)
        router.push('/login').then(() => {})
      } else {
        router.push('/login').then(() => {})
      }
      return true
    }
    return false
  },
  // 权限不足
  data => {
    if (data.code === ResultCode.CODE_DENY_ACCESS) {
      return true
    }
  }
]

export default AutoError

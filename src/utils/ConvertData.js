/**
 * 格式化时间格式
 * @param {Array} time 时间数组
 */
export const formatTime = (time) => {
  if (!time) {
    return ''
  }
  for (let i = 1; i < time.length; i++) {
    time[i] = time[i] < 10 ? '0' + time[i] : '' + time[i]
  }
  return time[0] + '-' + time[1] + '-' + time[2] + ' ' + time[3] + ':' + time[4] + ':' + time[5]
}

/* 免登录白名单页面 */
const whiteList = [
  '/',
  '/index',
  '/login',
  '/register',
  '/error/403',
  '/error/404',
  '/error/500'
]

export default whiteList

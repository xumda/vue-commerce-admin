import mainRouter from './mainRouter'

// 静态页面路由
const staticRouter = [
  {
    path: '/',
    redirect: '/login'
  }, {
    path: '/index',
    name: 'index',
    component: () => import('@/views/layout/TheLayout'),
    children: mainRouter
  }, {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/AppLogin')
  }, {
    path: '/register',
    name: 'register',
    component: () => import('@/views/login/AppRegister')
  }, {
    path: '/error/403',
    name: 'error403',
    component: () => import('@/views/error/AppError403')
  }, {
    path: '/error/500',
    name: 'error500',
    component: () => import('@/views/error/AppError500')
  }, {
    path: '*',
    name: 'error404',
    component: () => import('@/views/error/AppError404')
  }
]

export default staticRouter

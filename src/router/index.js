import Vue from 'vue'
import Router from 'vue-router'
import whiteList from '@/router/whiteList.js'
import staticRouter from '@/router/staticRouter'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  routes: staticRouter
})

// 权限控制
router.beforeEach((to, from, next) => {
  /* 免登录页面 */
  if (whiteList.indexOf(to.fullPath) >= 0) {
    return next()
  }
  return next()
})

export default router

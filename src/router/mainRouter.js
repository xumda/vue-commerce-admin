const mainRouter = [
  {
    path: '',
    name: 'home',
    component: () => import('@pages/home/PageHome')
  }, {
    path: 'storeCategory',
    name: 'storeCategory',
    component: () => import('@pages/store/PageStoreCategory')
  }, {
    path: 'goodsList',
    name: 'goodsList',
    component: () => import('@pages/goods/PageGoodsList')
  }, {
    path: 'goodCategory',
    name: 'goodCategory',
    component: () => import('@pages/goods/PageGoodCategory')
  }, {
    path: 'goodsCheck',
    name: 'goodsCheck',
    component: () => import('@pages/goods/PageGoodsCheck')
  }, {
    path: 'orderList',
    name: 'orderList',
    component: () => import('@pages/order/PageOrderList')
  }, {
    path: 'pageIndexCarousel',
    name: 'pageIndexCarousel',
    component: () => import('@pages/page/PageIndexCarousel')
  }, {
    path: 'pageIndexFeatured',
    name: 'pageIndexFeatured',
    component: () => import('@pages/page/PageIndexFeatured')
  }, {
    path: 'pageAccount',
    name: 'pageAccount',
    component: () => import('@pages/resource/PageAccount')
  }, {
    path: 'pageMenu',
    name: 'pageMenu',
    component: () => import('@pages/resource/PageMenu')
  }, {
    path: 'pageRole',
    name: 'pageRole',
    component: () => import('@pages/resource/PageRole')
  }, {
    path: 'pageSolidifyRole',
    name: 'pageSolidifyRole',
    component: () => import('@pages/resource/PageSolidifyRole')
  }, {
    path: 'pagePermission',
    name: 'pagePermission',
    component: () => import('@pages/resource/PagePermission')
  }, {
    path: 'testPage1',
    name: 'testPage1',
    component: () => import('@pages/test/PageTest1')
  }
]

export default mainRouter

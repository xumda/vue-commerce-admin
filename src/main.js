import Vue from 'vue'
import App from '@/App.vue'
import VCharts from 'v-charts'
import router from '@/router/index'
import store from '@/store/index'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/common.scss'
import { startSessionRegister } from '@/api/ApiInit'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(VCharts)

// noinspection JSUnusedGlobalSymbols
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// 开始session同步心跳
startSessionRegister()

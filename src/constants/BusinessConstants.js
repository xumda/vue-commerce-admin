const BusinessConstants = {
  // 消息类型，系统消息
  MESSAGE_TYPE_SYSTEM: 'SYSTEM',
  // 消息类型，个人消息
  MESSAGE_TYPE_PERSON: 'PERSON',
  // 消息消费，已消费
  MESSAGE_CONSUME_TRUE: 1,
  // 消息消费，未消费
  MESSAGE_CONSUME_FALSE: 0,
  // 权限创建模式，手动创建 HAND
  PERMISSION_CREATE_MODE_HAND: 'HAND',
  // 权限创建模式，自动创建 SELF
  PERMISSION_CREATE_MODE_SELF: 'SELF',
  // 权限分配对象，用户
  PERMISSION_ALLOT_PART_USER: 'USER',
  // 权限分配对象，角色
  PERMISSION_ALLOT_PART_ROLE: 'ROLE',
  // 权限分配对象，菜单
  PERMISSION_ALLOT_PART_MENU: 'MENU',
  // 权限和菜单分配模式，手动分配 HAND
  ALLOT_MODE_HAND: 'HAND',
  // 权限和菜单分配模式，自动分配 SELF
  ALLOT_MODE_SELF: 'SELF',
  // 用户类型，普通用户
  USER_TYPE_USER: 'USER',
  // 用户类型，店铺
  USER_TYPE_STORE: 'STORE',
  // 用户类型，管理员
  USER_TYPE_ADMIN: 'ADMIN'
}

export default BusinessConstants

export const GoodsState = {
  STATE_CHECK_PENDING: '待审核',
  STATE_CHECKING: '审核中',
  STATE_FAILED: '审核失败',
  STATE_NOT_ON: '未上架',
  STATE_NORMAL: '正常'
}

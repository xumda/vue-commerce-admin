import { dbConstants, ResultCode } from '@/constants/constants'

const constants = {
  dbConstants: dbConstants,
  resultCode: ResultCode
}

export default constants

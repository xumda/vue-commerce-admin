export const dbConstants = {
  bindingType: {
    QQ: 1,
    WeChat: 2,
    Alipay: 3,
    Email: 4,
    Phone: 5,
    IdCard: 6
  }
}

export const ResultCode = {
  OK: 200,
  CODE_NOT_LOGIN: 401
}

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
import { fallbackRequestPost } from '@/utils/Request'

const URL_PREFIX = 'middle-goods/'

/**
 * Admin权限，获取商品类别信息，结果表示为一棵树
 * @param {PageDTO} pageDTO 分页查询条件
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryAdminInfoByPageWrapper = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'store/queryAdminInfoByPageWrapper', pageDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

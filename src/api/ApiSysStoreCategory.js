/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

const URL_PREFIX = 'middle-goods/'

/**
 * 获取商品类别信息，结果表示为一棵树
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryStoreCategoryTree = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'sys_store_category/queryTree', {}, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 添加一个店铺分类
 * @param {StoreCategoryInsertDTO} storeCategoryInsertDTO 新添加的店铺分类数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const insertStoreCategory = (storeCategoryInsertDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'sys_store_category/insertStoreCategory', storeCategoryInsertDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 更新一个店铺分类
 * @param {StoreCategoryUpdateDTO} storeCategoryUpdateDTO 需要更新的店铺分类数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const updateStoreCategory = (storeCategoryUpdateDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'sys_store_category/updateStoreCategory', storeCategoryUpdateDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id删除店铺分类，只删除该节点，其子节点全部放到父节点中去
 * @param {String|Number} id 店铺分类id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeStoreCategoryById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'sys_store_category/removeStoreCategoryById', { id }, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id删除店铺分类，只删除该节点，其子节点全部放到父节点中去
 * @param {Array<String|Number>} ids 店铺分类id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeStoreCategoryByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'sys_store_category/removeStoreCategoryByIds', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id删除店铺分类，该操作会移除当前节点及其所有子孙节点
 * @param {String|Number} id 店铺分类id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeStoreCategoryAllById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'sys_store_category/removeStoreCategoryAllById', { id }, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id删除店铺分类，该操作会移除当前节点及其所有子孙节点
 * @param {Array<String|Number>} ids 店铺分类id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeStoreCategoryAllByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'sys_store_category/removeStoreCategoryAllByIds', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

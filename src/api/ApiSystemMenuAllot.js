import { fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 分配菜单
 * @param {Array<SystemMenuAllotDTO>} list 菜单操作DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否保存成功
 */
export const allotMenu = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'system_menu_allot/allotMenu', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 移除菜单
 * @param {Array<SystemMenuAllotDTO>} list 菜单操作DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否移除成功
 */
export const removeMenuAllot = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'system_menu_allot/removeMenuAllot', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据分配编号移除菜单分配
 * @param {Array<Number|String>} list 分配编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否移除成功
 */
export const removeMenuAllotByIds = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'system_menu_allot/removeMenuAllotByIds', list, {}, true,
    successCallback, null, fullCallback)
}

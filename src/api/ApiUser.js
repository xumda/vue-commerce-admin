import { fallbackRequestPost } from '@/utils/Request'
import { dbConstants } from '@/constants/constants'
import * as Message from 'element-ui'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 账号密码登陆
 * @param {String} username 用户名，目前只支持邮箱和手机
 * @param {String} password 登陆密码
 * @param {String} code 验证码
 * @param {Function} errorCallback 失败回调，参数为data
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const loginByPass = (username, password, code, successCallback = null, errorCallback = null, fullCallback = null) => {
  const params = {
    'username': username,
    'password': password,
    'userType': 'ADMIN',
    'type': username.indexOf('@') === -1 ? dbConstants.bindingType.Phone : dbConstants.bindingType.Email,
    'code': code
  }

  return fallbackRequestPost(URL_PREFIX + 'user/login', params, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, errorCallback, fullCallback)
}

export const requestRegister = () => {
  Message.error('暂不支持注册功能，请自行前往客户端申请店铺资格')
}

import { fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 分配权限
 * @param {Array<PermissionAllotDTO>} list 权限操作DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const allotPermission = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'permission_allot/allotPermission', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 移除权限
 * @param {Array<PermissionAllotDTO>} list 权限操作DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const removePermission = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'permission_allot/removePermission', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据分配编号移除权限
 * @param {Array<String|Number>} list 分配编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const removePermissionByIds = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'permission_allot/removePermissionByIds', list, {}, true,
    successCallback, null, fullCallback)
}

import { fallbackRequestGet } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 根据用户类型查询人员基本信息，发送消息时使用
 * @param type 用户类型
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 用户信息列表
 */
export const queryUserByType = (type, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'userInfo/queryUserByType', {
    type: type
  }, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

import * as QiNiu from 'qiniu-js/src'
import { MessageBox } from 'element-ui'

/**
 * 文件上传
 * @param file 文件
 * @param name 文件名
 * @param token 上传凭证
 * @param {Object} putExtra 配置数据
 * @param {Object} config 额外的数据
 * @param next 接收上传进度信息，res 参数是一个带有 total 字段的 object，包含loaded、total、percent三个属性，提供上传进度信息。
 *                    total.loaded: number，已上传大小，单位为字节。
 *                    total.total: number，本次上传的总量控制信息，单位为字节，注意这里的 total 跟文件大小并不一致。
 *                    total.percent: number，当前上传进度，范围：0～100。
 * @param error 上传错误后触发，当不是 xhr 请求错误时，会把当前错误产生原因直接抛出，诸如 JSON 解析异常等；
 *              当产生 xhr 请求错误时，参数 err 为一个包含 code、message、isRequestError 三个属性的 object
 *                    err.isRequestError: 用于区分是否 xhr 请求错误；当 xhr 请求出现错误并且后端通过 HTTP 状态码返回了错误信息时，该参数为 true；否则为 undefined 。
 *                    err.reqId: string，xhr请求错误的 X-Reqid。
 *                    err.code: number，请求错误状态码，只有在 err.isRequestError 为 true 的时候才有效，可查阅码值对应说明。
 *                    err.message: string，错误信息，包含错误码，当后端返回提示信息时也会有相应的错误信息。
 * @param complete 接收上传完成后的后端返回信息，res 参数为一个 object， 为上传成功后后端返回的信息，具体返回结构取决于后端sdk的配置，可参考上传策略。
 * @return {Object} 创造控制observer和subscribeAction的实例对象
 */
export const uploadFile = (file, name, token, putExtra = null, config = null, next = null, error = null, complete = null) => {
  const observable = QiNiu.upload(file, name, token, putExtra, config)
  return observable.subscribe(next, error, complete)
}

/**
 * 文件上传，自动处理隐藏
 * @param file 文件
 * @param name 文件名
 * @param token 上传凭证
 * @param {Object} putExtra 配置数据
 * @param {Object} config 额外的数据
 * @param next 接收上传进度信息，res 参数是一个带有 total 字段的 object，包含loaded、total、percent三个属性，提供上传进度信息。
 *                    total.loaded: number，已上传大小，单位为字节。
 *                    total.total: number，本次上传的总量控制信息，单位为字节，注意这里的 total 跟文件大小并不一致。
 *                    total.percent: number，当前上传进度，范围：0～100。
 * @param complete 接收上传完成后的后端返回信息，res 参数为一个 object， 为上传成功后后端返回的信息，具体返回结构取决于后端sdk的配置，可参考上传策略。
 * @param error 上传错误后触发，当不是 xhr 请求错误时，会把当前错误产生原因直接抛出，诸如 JSON 解析异常等；
 *              当产生 xhr 请求错误时，参数 err 为一个包含 code、message、isRequestError 三个属性的 object
 *                    err.isRequestError: 用于区分是否 xhr 请求错误；当 xhr 请求出现错误并且后端通过 HTTP 状态码返回了错误信息时，该参数为 true；否则为 undefined 。
 *                    err.reqId: string，xhr请求错误的 X-Reqid。
 *                    err.code: number，请求错误状态码，只有在 err.isRequestError 为 true 的时候才有效，可查阅码值对应说明。
 *                    err.message: string，错误信息，包含错误码，当后端返回提示信息时也会有相应的错误信息。
 * @return {Object} 创造控制observer和subscribeAction的实例对象
 */
export const uploadFileAutoErr = (file, name, token, putExtra = null, config = null, next = null, complete = null, error = null) => {
  uploadFile(file, name, token, putExtra, config, next, err => {
    if (error) {
      error(err)
    }
    const errMsg = err.message || '未知错误，请联系管理员！'
    const errCod = err.code
    MessageBox.alert(errMsg, '请求异常：' + errCod, {
      confirmButtonText: '确定',
      type: 'error',
      callback: () => {
      }
    }).then()
  }, complete)
}

/**
 * 取消上传
 * @param subscription 创造控制observer和subscribeAction的实例对象
 */
export const unsubscribe = (subscription) => {
  subscription.unsubscribe()
}

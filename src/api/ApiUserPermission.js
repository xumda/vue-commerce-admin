import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 根据账号查询用户权限
 * @param {Number|String} account 用户账户
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 权限列表
 */
export const queryPermissionsByAccount = (account, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'user_permission/queryPermissionsByAccount', { account }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据账号查询用户权限
 * @param {Number|String} account 用户账户
 * @param {Number|String} userType 用户账户
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 权限列表
 */
export const queryPermissionsByAccountAndType = (account, userType, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'user_permission/queryPermissionsByAccountAndType', { account, userType }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询当前登陆账户的权限
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 权限列表
 */
export const queryCurrentPermissions = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'user_permission/queryCurrentPermissions', { }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询当前登陆账户的权限
 * @param {Array<Number|String>} accounts 账户列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 权限列表
 */
export const queryUsersPermissions = (accounts, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'user_permission/queryUsersPermissions', accounts, {}, true,
    successCallback, null, fullCallback)
}

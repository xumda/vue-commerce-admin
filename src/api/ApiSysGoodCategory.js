/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

const URL_PREFIX = 'middle-goods/'

/**
 * 获取商品类别信息，结果表示为一棵树
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryGoodCategoryTree = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'sys_good_category/queryTree', {}, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 添加一个商品分类
 * @param {GoodCategoryInsertDTO} goodCategoryInsertDTO 新添加的商品分类数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const insertGoodCategory = (goodCategoryInsertDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'sys_good_category/insertGoodCategory', goodCategoryInsertDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 更新一个商品分类
 * @param {GoodCategoryUpdateDTO} goodCategoryUpdateDTO 需要更新的商品分类数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const updateGoodCategory = (goodCategoryUpdateDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'sys_good_category/updateGoodCategory', goodCategoryUpdateDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id删除商品分类，只删除该节点，其子节点全部放到父节点中去
 * @param {String|Number} id 商品分类id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeGoodCategoryById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'sys_good_category/removeGoodCategoryById', { id }, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id删除商品分类，只删除该节点，其子节点全部放到父节点中去
 * @param {Array<String|Number>} ids 商品分类id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeGoodCategoryByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'sys_good_category/removeGoodCategoryByIds', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id删除商品分类，删除该节点即其所有子孙节点
 * @param {String|Number} id 商品分类id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeGoodCategoryAllById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'sys_good_category/removeGoodCategoryAllById', { id }, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id删除商品分类，删除该节点即其所有子孙节点
 * @param {Array<String|Number>} ids 商品分类id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeGoodCategoryAllByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'sys_good_category/removeGoodCategoryAllByIds', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

const URL_PREFIX = 'middle-goods/'

/**
 * 获取首页走马灯数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryCarouselVO = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'pageIndexController/queryCarouselVO', {}, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 添加保存一条首页的Carousel数据
 * @param {PageIndexCarouselInsertDTO} pageIndexCarouselInsertDTO 添加数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const insertIndexCarousel = (pageIndexCarouselInsertDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'pageIndexController/insertIndexCarousel', pageIndexCarouselInsertDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 修改首页轮播图数据
 * @param {PageIndexCarouselChangeDTO} pageIndexCarouselChangeDTO 轮播图数据修改DTO
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const changeIndexCarousel = (pageIndexCarouselChangeDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'pageIndexController/changeIndexCarousel', pageIndexCarouselChangeDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id移除数据
 * @param {Array<Number|String>} list 轮播图数据编号列表
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeIndexCarouselByIds = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'pageIndexController/removeIndexCarouselByIds', list, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 获取首页Featured数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryFeaturedVO = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'pageIndexController/queryFeaturedVO', {}, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 添加保存一条首页的Featured数据
 * @param {PageIndexFeaturedInsertDTO} pageIndexFeaturedInsertDTO 添加数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const insertIndexFeatured = (pageIndexFeaturedInsertDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'pageIndexController/insertIndexFeatured', pageIndexFeaturedInsertDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 修改首页Featured数据
 * @param {PageIndexFeaturedChangeDTO} pageIndexFeaturedChangeDTO Featured数据修改DTO
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const changeIndexFeatured = (pageIndexFeaturedChangeDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'pageIndexController/changeIndexFeatured', pageIndexFeaturedChangeDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

import { fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 分配店铺角色
 * @param {Array<UserRoleInsertDTO>} list 角色操作DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否保存成功
 */
export const allotStoreRole = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'user_role/allotStoreRole', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 分配角色
 * @param {Array<UserRoleInsertDTO>} list 角色操作DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否保存成功
 */
export const allotAdminRole = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'user_role/allotAdminRole', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 移除店铺角色
 * @param {Array<UserRoleRemoveDTO>} list 角色操作DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否保存成功
 */
export const removeStoreRoleAllot = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'user_role/removeStoreRoleAllot', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 移除管理系统角色
 * @param {Array<UserRoleRemoveDTO>} list 角色操作DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否保存成功
 */
export const removeAdminRoleAllot = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'user_role/removeAdminRoleAllot', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据分配编号移除店铺角色分配
 * @param {Array<String|Number>} list 分配编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否保存成功
 */
export const removeStoreRoleAllotByIds = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'user_role/removeStoreRoleAllotByIds', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据分配编号移除管理系统角色分配
 * @param {Array<String|Number>} list 分配编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否保存成功
 */
export const removeAdminRoleAllotByIds = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'user_role/removeAdminRoleAllotByIds', list, {}, true,
    successCallback, null, fullCallback)
}

import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-goods/'

/**
 * 根据商品id获取商品详细信息，店铺端调用
 * @param {String|Number} id 商品id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryGoodDetailByIdStore = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'good_detail/queryByIdStore', {
    id: id
  }, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据商品id获取商品详细信息，Admin权限端调用
 * @param {String|Number} id 商品id
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryGoodDetailByIdAdmin = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'good_detail/queryByIdAdmin', {
    id: id
  }, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 后台管理员调用，根据条件筛选商品信息，查询所有商品信息
 * @param {PageDTO<GoodDetailQueryDTO>} pageDTO 查询条件DTO, 包含查询条件，查询条件可以为空，为空时无条件进行查询
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryGoodDetailByPageAdmin = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'good_detail/queryGoodDetailByPageAdmin', pageDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 添加单独的权限，超级管理员调用
 * @param {Array<PermissionInsertDTO>} insertList 权限添加数据DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const insertPermission = (insertList, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'permission/insertPermission', insertList, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据权限编号修改权限信息，超级管理员调用
 * @param {Array<PermissionChangeDTO>} changeList 权限修改数据DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const changePermissionById = (changeList, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'permission/changePermissionById', changeList, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据权限编号删除权限信息，超级管理员调用
 * @param {Array<Number|String>} permissionIdList 权限编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const removePermission = (permissionIdList, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'permission/removePermission', permissionIdList, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据权限编号查询单个权限信息，超级管理员调用
 * @param {Number|String} id 权限编号
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const queryPermissionById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'permission/queryPermissionById', { id }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据多个权限编号查询多个权限信息，超级管理员调用
 * @param {Array<Number|String>} ids 权限编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const queryPermissionByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'permission/queryPermissionByIds', ids, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 条件分页查询权限数据，超级管理员调用
 * @param {PageDTO<PermissionQueryDTO>} pageDTO 分页条件DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 结果
 */
export const queryPermissionPageWrapper = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'permission/queryPermissionPageWrapper', pageDTO, {}, true,
    successCallback, null, fullCallback)
}

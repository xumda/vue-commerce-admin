import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 保存一条发送的消息
 * @param {Array<MessageInsertDTO>} list 保存消息使用DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 返回保存后的数据
 */
export const addMessages = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'message/addMessages', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据消息id查询消息
 * @param {String|Number} id 消息id
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 消息数据
 */
export const queryById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'message/queryById', { id }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据多个id查询消息数据
 * @param {Array<String|Number>} ids 多个消息id
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 消息数据
 */
export const queryByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'message/queryByIds', ids, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据条件分页查询消息数据
 * @param {PageDTO<MessageQueryDTO>} pageDTO 分页DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 分页数据
 */
export const queryByPageWrapper = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'message/queryByPageWrapper', pageDTO, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 分页条件查询未消费的消息数据
 * @param {PageDTO<MessageQueryDTO>} pageDTO 分页DTO
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 分页数据
 */
export const queryNoConsumeByPageWrapper = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'message/queryNoConsumeByPageWrapper', pageDTO, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询所以的未消费消息数据
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 所有未消费的消息数据
 */
export const queryAllNoConsume = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'message/queryAllNoConsume', {}, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询未消费消息数据个数
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 未消费消息个数
 */
export const queryNoConsumeCount = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'message/queryNoConsumeCount', {}, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据id消费消息数据
 * @param {Array<String|Number>} ids 多个消息数据编号
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 消费后的消息数据
 */
export const consumeMessageByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'message/consumeMessageByIds', ids, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 消费所有消息数据
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 消费所有消息数据
 */
export const consumeAll = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'message/consumeAll', {}, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 删除消息
 * @param {Array<String|Number>} ids 多个消息数据编号
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 移除的消息
 */
export const removeByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'message/removeByIds', ids, {}, true,
    successCallback, null, fullCallback)
}

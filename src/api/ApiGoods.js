/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

const URL_PREFIX = 'middle-goods/'

/**
 * 获取用于表格筛选的条目
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryStoreGoodTableFilter = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'goods/queryStoreGoodTableFilter', {}, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 添加商品数据
 * @param {GoodsInsertDTO} goodsInsertDTO 参数DTO
 * @param {Array<File>} files 图片文件
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const addGood = (goodsInsertDTO, files, successCallback = null, fullCallback = null) => {
  const config = {
    headers: { 'Content-Type': 'multipart/form-data' }
  }
  const formDate = new FormData()
  files.forEach(item => {
    formDate.append('files', item)
  })
  formDate.append('groupId', goodsInsertDTO.getGroupId())
  formDate.append('name', goodsInsertDTO.getName())
  formDate.append('simpleDesc', goodsInsertDTO.getSimpleDesc())
  formDate.append('description', goodsInsertDTO.getDescription())
  formDate.append('goodCategoryId', goodsInsertDTO.getGoodCategoryId())
  formDate.append('sellPoint', goodsInsertDTO.getSellPoint())
  formDate.append('freight', goodsInsertDTO.getFreight())
  formDate.append('remark', goodsInsertDTO.getRemark())
  const list = goodsInsertDTO.getSubGoodList()
  let subList
  for (let i = 0; i < list.length; i++) {
    subList = list[i]
    formDate.append('subGoodList[' + i + '].subGoodName', subList.getSubGoodName())
    formDate.append('subGoodList[' + i + '].description', subList.getDescription())
    formDate.append('subGoodList[' + i + '].count', subList.getCount())
    formDate.append('subGoodList[' + i + '].price', subList.getPrice())
    formDate.append('subGoodList[' + i + '].remark', subList.getRemark())
  }
  return fallbackRequestPost(URL_PREFIX + 'goods/addGood', formDate, config, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 修改商品信息
 * @param {GoodsChangeDTO} goodsChangeDTO 参数DTO
 * @param {Array<File>} files 图片文件
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const changeGood = (goodsChangeDTO, files, successCallback = null, fullCallback = null) => {
  const config = {
    headers: { 'Content-Type': 'multipart/form-data' }
  }
  const formDate = new FormData()
  files.forEach(item => {
    formDate.append('files', item)
  })
  formDate.append('id', goodsChangeDTO.getId())
  formDate.append('groupId', goodsChangeDTO.getGroupId())
  formDate.append('name', goodsChangeDTO.getName())
  formDate.append('simpleDesc', goodsChangeDTO.getSimpleDesc())
  formDate.append('description', goodsChangeDTO.getDescription())
  formDate.append('goodCategoryId', goodsChangeDTO.getGoodCategoryId())
  formDate.append('sellPoint', goodsChangeDTO.getSellPoint())
  formDate.append('freight', goodsChangeDTO.getFreight())
  formDate.append('remark', goodsChangeDTO.getRemark())
  const list = goodsChangeDTO.getSubGoodList()
  let subList
  for (let i = 0; i < list.length; i++) {
    subList = list[i]
    if (subList.getId()) {
      formDate.append('subGoodList[' + i + '].id', subList.getId())
    }
    formDate.append('subGoodList[' + i + '].subGoodName', subList.getSubGoodName())
    formDate.append('subGoodList[' + i + '].description', subList.getDescription())
    formDate.append('subGoodList[' + i + '].count', subList.getCount())
    formDate.append('subGoodList[' + i + '].price', subList.getPrice())
    formDate.append('subGoodList[' + i + '].remark', subList.getRemark())
  }
  if (goodsChangeDTO.getRemoveSubGoods()) {
    goodsChangeDTO.getRemoveSubGoods().forEach(item => {
      formDate.append('removeSubGoods', item + '')
    })
  }
  if (goodsChangeDTO.getRemoveFiles()) {
    goodsChangeDTO.getRemoveFiles().forEach(item => {
      formDate.append('removeFiles', item + '')
    })
  }
  return fallbackRequestPost(URL_PREFIX + 'goods/changeGood', formDate, config, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 根据id批量删除商品信息
 * @param {Array<Number>} ids 商品id数组
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const removeGoodsByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'goods/removeGoodsByIds', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 查询商品基本信息，条件分页查询
 * @param {PageDTO} pageDTO 分页条件数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryGoodsByPageWrapperStore = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'goods/queryGoodsByPageWrapperStore', pageDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 查询商品基本信息，条件分页查询
 * @param {PageDTO} pageDTO 分页条件数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryGoodsByPageWrapperAdmin = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'goods/queryGoodsByPageWrapperAdmin', pageDTO, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 提交商品审核
 * @param {Array<Number|String>} ids 商品id数组
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const pushGoodsCheckPending = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'goods/pushGoodsCheckPending', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 取消商品审核
 * @param {Array<Number|String>} ids 商品id数组
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const cancelGoodsCheckPending = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'goods/cancelGoodsCheckPending', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 上架商品
 * @param {Array<Number|String>} ids 商品id数组
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const pushGoods = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'goods/pushGoods', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 下架商品
 * @param {Array<Number|String>} ids 商品id数组
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const pullGoods = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'goods/pullGoods', ids, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * 商品审核
 * @param {Array<AdminCheckingDTO>} params 审核数据
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const checkingGoods = (params, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'goods/checkingGoods', params, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

/**
 * todo 测试
 * @param files
 * @param name
 * @param successCallback
 * @param fullCallback
 * @return {*}
 */
export const testFileUpload = (files, name, successCallback = null, fullCallback = null) => {
  const config = {
    headers: { 'Content-Type': 'multipart/form-data' }
  }
  const formDate = new FormData()
  files.forEach(item => {
    formDate.append('files', item)
  })
  formDate.append('name', name)
  return fallbackRequestPost(URL_PREFIX + 'goods/testFileUpload', formDate, config, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

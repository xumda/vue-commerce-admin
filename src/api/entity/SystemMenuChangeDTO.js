export default class SystemMenuChangeDTO {
  static instance () {
    return new SystemMenuChangeDTO()
  }

  /**
   * 菜单编号
   * @param {String|Number} menuId
   * @return {SystemMenuChangeDTO}
   */
  setMenuId (menuId) {
    if (menuId) {
      this.menuId = menuId
    }
    return this
  }

  /**
   * 菜单名称
   * @param {String|Number} menuName
   * @return {SystemMenuChangeDTO}
   */
  setMenuName (menuName) {
    if (menuName) {
      this.menuName = menuName
    }
    return this
  }

  /**
   * 菜单图标
   * @param {String|Number} menuIcon
   * @return {SystemMenuChangeDTO}
   */
  setMenuIcon (menuIcon) {
    if (menuIcon) {
      this.menuIcon = menuIcon
    }
    return this
  }

  /**
   * 菜单编码
   * @param {String|Number} menuCode
   * @return {SystemMenuChangeDTO}
   */
  setMenuCode (menuCode) {
    if (menuCode) {
      this.menuCode = menuCode
    }
    return this
  }

  /**
   * 菜单链接路径
   * @param {String|Number} menuUrl
   * @return {SystemMenuChangeDTO}
   */
  setMenuUrl (menuUrl) {
    if (menuUrl) {
      this.menuUrl = menuUrl
    }
    return this
  }

  /**
   * 排序字段，小数在前
   * @param {String|Number} menuOrder
   * @return {SystemMenuChangeDTO}
   */
  setMenuOrder (menuOrder) {
    if (menuOrder) {
      this.menuOrder = menuOrder
    }
    return this
  }

  /**
   * 菜单描述
   * @param {String|Number} menuDesc
   * @return {SystemMenuChangeDTO}
   */
  setMenuDesc (menuDesc) {
    if (menuDesc) {
      this.menuDesc = menuDesc
    }
    return this
  }

  /**
   * 父节点编号，第一级节点为子系统短码
   * @param {String|Number} parentMenuId
   * @return {SystemMenuChangeDTO}
   */
  setParentMenuId (parentMenuId) {
    if (parentMenuId) {
      this.parentMenuId = parentMenuId
    }
    return this
  }
}

export default class SystemMenuAllotDTO {
  static instance () {
    return new SystemMenuAllotDTO()
  }

  /**
   * 分配对象，用户，角色
   * @param {String|Number} allotPart
   * @return {SystemMenuAllotDTO}
   */
  setAllotPart (allotPart) {
    if (allotPart) {
      this.allotPart = allotPart
    }
    return this
  }

  /**
   * 账户编号或角色编号
   * @param {String|Number} partId
   * @return {SystemMenuAllotDTO}
   */
  setPartId (partId) {
    if (partId) {
      this.partId = partId
    }
    return this
  }

  /**
   * 菜单列表
   * @param {Array<String|Number>} menuIds
   * @return {SystemMenuAllotDTO}
   */
  setMenuIds (menuIds) {
    if (menuIds) {
      this.menuIds = menuIds
    }
    return this
  }

  /**
   * 菜单列表
   * @param {String|Number} menuIds
   * @return {SystemMenuAllotDTO}
   */
  addMenuIds (menuIds) {
    if (menuIds) {
      if (this.menuIds) {
        this.menuIds.push(menuIds)
      } else {
        this.menuIds = [menuIds]
      }
    }
    return this
  }
}

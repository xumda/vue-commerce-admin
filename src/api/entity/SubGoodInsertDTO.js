export default class SubGoodInsertDTO {
  static instance () {
    return new SubGoodInsertDTO()
  }

  getSubGoodName () {
    return this.subGoodName
  }

  setSubGoodName (subGoodName) {
    this.subGoodName = subGoodName
  }

  getDescription () {
    return this.description
  }

  setDescription (description) {
    this.description = description
  }

  getCount () {
    return this.count
  }

  setCount (count) {
    this.count = count
  }

  getPrice () {
    return this.price
  }

  setPrice (price) {
    this.price = price
  }

  getRemark () {
    return this.remark
  }

  setRemark (remark) {
    this.remark = remark
  }
}

export default class GoodsChangeDTO {
  static instance () {
    return new GoodsChangeDTO()
  }

  getId () {
    return this.id
  }

  getGroupId () {
    return this.groupId
  }

  getName () {
    return this.name
  }

  getSimpleDesc () {
    return this.simpleDesc
  }

  getDescription () {
    return this.description
  }

  getGoodCategoryId () {
    return this.goodCategoryId
  }

  getSellPoint () {
    return this.sellPoint
  }

  getFreight () {
    return this.freight
  }

  getRemark () {
    return this.remark
  }

  /**
   * @return {Array<SubGoodChangeDTO>}
   */
  getSubGoodList () {
    return this.subGoodList
  }

  /**
   * 将要移除的图片记录id列表
   * @return {Array<Number>}
   */
  getRemoveFiles () {
    return this.removeFiles
  }

  /**
   * 将要移除的子商品id列表
   * @return {Array<String | Number>}
   */
  getRemoveSubGoods () {
    return this.removeSubGoods
  }

  setId (id) {
    this.id = id
  }

  setGroupId (groupId) {
    this.groupId = groupId
  }

  setName (name) {
    this.name = name
  }

  setSimpleDesc (simpleDesc) {
    this.simpleDesc = simpleDesc
  }

  setDescription (description) {
    this.description = description
  }

  setGoodCategoryId (goodCategoryId) {
    this.goodCategoryId = goodCategoryId
  }

  setSellPoint (sellPoint) {
    this.sellPoint = sellPoint
  }

  setFreight (freight) {
    this.freight = freight
  }

  setRemark (remark) {
    this.remark = remark
  }

  setSubGoodList (subGoodList) {
    this.subGoodList = subGoodList
  }

  setRemoveFiles (removeFiles) {
    this.removeFiles = removeFiles
  }

  setRemoveSubGoods (removeSubGoods) {
    this.removeSubGoods = removeSubGoods
  }
}

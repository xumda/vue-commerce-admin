export default class GoodsAdminQueryDTO {
  static instance () {
    return new GoodsAdminQueryDTO()
  }

  /**
   * 搜索类型，如普通所有，店铺搜索，类别搜索等
   */
  setSearchType (val) {
    if (val) {
      this.searchType = val
    }
  }

  /**
   * 搜索字符串
   */
  setSearch (val) {
    if (val) {
      this.search = val
    }
  }

  /**
   * 商品id，精确查询
   */
  setId (val) {
    if (val) {
      this.id = val
    }
  }

  /**
   * 商品分组id，精确查询
   */
  setGroupId (val) {
    if (val) {
      this.groupId = val
    }
  }

  /**
   * 分组名称, 模糊匹配
   */
  setGroupName (val) {
    if (val) {
      this.groupName = val
    }
  }

  /**
   * 店铺id，精确查询
   */
  setStoreId (val) {
    if (val) {
      this.storeId = val
    }
  }

  /**
   * 店铺名称, 模糊匹配
   */
  setStoreName (val) {
    if (val) {
      this.storeName = val
    }
  }

  /**
   * 创建人员id，精确查询
   */
  setCreateUserId (val) {
    if (val) {
      this.createUserId = val
    }
  }

  /**
   * 创建人员名称, 模糊匹配
   */
  setCreateUserName (val) {
    if (val) {
      this.createUserName = val
    }
  }

  /**
   * 更新人员id
   */
  setUpdateUserId (val) {
    if (val) {
      this.updateUserId = val
    }
  }

  /**
   * 更新人员名称
   */
  setUpdateUserName (val) {
    if (val) {
      this.updateUserName = val
    }
  }

  /**
   * 删除人员id
   */
  setRemoveUserId (val) {
    if (val) {
      this.removeUserId = val
    }
  }

  /**
   * 删除人员名称
   */
  setRemoveUserName (val) {
    if (val) {
      this.removeUserName = val
    }
  }

  /**
   * 创建本记录的时间，范围值
   */
  setCreateTimeBegin (val) {
    if (val) {
      this.createTimeBegin = val
    }
  }

  /**
   * 更新本记录的时间，范围值
   */
  setUpdateTimeBegin (val) {
    if (val) {
      this.updateTimeBegin = val
    }
  }

  /**
   * 删除本记录的时间，范围值
   */
  setRemoveTimeBegin (val) {
    if (val) {
      this.removeTimeBegin = val
    }
  }

  /**
   * 创建本记录的时间，范围值
   */
  setCreateTimeEnd (val) {
    if (val) {
      this.createTimeEnd = val
    }
  }

  /**
   * 更新本记录的时间，范围值
   */
  setUpdateTimeEnd (val) {
    if (val) {
      this.updateTimeEnd = val
    }
  }

  /**
   * 删除本记录的时间，范围值
   */
  setRemoveTimeEnd (val) {
    if (val) {
      this.removeTimeEnd = val
    }
  }

  /**
   * 商品名称，模糊匹配
   */
  setName (val) {
    if (val) {
      this.name = val
    }
  }

  /**
   * 分类id，系统分类
   */
  setGoodCategoryId (val) {
    if (val) {
      this.goodCategoryId = val
    }
  }

  /**
   * 分类id，系统分类
   */
  setGoodCategoryName (val) {
    if (val) {
      this.goodCategoryName = val
    }
  }

  /**
   * 买点
   */
  setSellPoint (val) {
    if (val) {
      this.sellPoint = val
    }
  }

  /**
   * 热度得分，范围值
   */
  setScoreBegin (val) {
    if (val) {
      this.scoreBegin = val
    }
  }

  /**
   * 热度得分，范围值
   */
  setScoreEnd (val) {
    if (val) {
      this.scoreEnd = val
    }
  }

  /**
   * 运费，为0时表示包邮，范围值
   */
  setFreightBegin (val) {
    if (val) {
      this.freightBegin = val
    }
  }

  /**
   * 运费，为0时表示包邮，范围值
   */
  setFreightEnd (val) {
    if (val) {
      this.freightEnd = val
    }
  }

  /**
   * 上架时间，范围值
   */
  setPushTimeBegin (val) {
    if (val) {
      this.pushTimeBegin = val
    }
  }

  /**
   * 上架时间，范围值
   */
  setPushTimeEnd (val) {
    if (val) {
      this.pushTimeEnd = val
    }
  }

  /**
   * 下架时间，范围值
   */
  setPullTimeBegin (val) {
    if (val) {
      this.pullTimeBegin = val
    }
  }

  /**
   * 下架时间，范围值
   */
  setPullTimeEnd (val) {
    if (val) {
      this.pullTimeEnd = val
    }
  }

  /**
   * 商品状态：待审核，审核中，审核失败，未上架，售卖中（正常），缺货中...
   */
  setState (val) {
    if (val) {
      this.state = val
    }
  }

  /**
   * 状态描述，可以是审核失败原因，模糊匹配
   */
  setStateDesc (val) {
    if (val) {
      this.stateDesc = val
    }
  }

  /**
   * 审核人员
   */
  setCheckUserId (val) {
    if (val) {
      this.checkUserId = val
    }
  }

  /**
   * 审核人员名称
   */
  setCheckUserName (val) {
    if (val) {
      this.checkUserName = val
    }
  }

  /**
   * 审核时间，范围值
   */
  setCheckTimeBegin (val) {
    if (val) {
      this.checkTimeBegin = val
    }
  }

  /**
   * 审核时间，范围值
   */
  setCheckTimeEnd (val) {
    if (val) {
      this.checkTimeEnd = val
    }
  }
}

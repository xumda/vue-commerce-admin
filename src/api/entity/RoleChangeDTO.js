export default class RoleChangeDTO {
  static instance () {
    return new RoleChangeDTO()
  }

  /**
   * 角色编号
   * @param {String|Number} roleId
   * @return {RoleChangeDTO}
   */
  setRoleId (roleId) {
    if (roleId) {
      this.roleId = roleId
    }
    return this
  }

  /**
   * 角色名称
   * @param {String|Number} roleName
   * @return {RoleChangeDTO}
   */
  setRoleName (roleName) {
    if (roleName) {
      this.roleName = roleName
    }
    return this
  }

  /**
   * 角色描述
   * @param {String|Number} roleDesc
   * @return {RoleChangeDTO}
   */
  setRoleDesc (roleDesc) {
    if (roleDesc) {
      this.roleDesc = roleDesc
    }
    return this
  }

  /**
   * 角色有效开始时间，时间段以为将无法享受角色权限
   * @param {String|Number} roleBeginTime
   * @return {RoleChangeDTO}
   */
  setRoleBeginTime (roleBeginTime) {
    if (roleBeginTime) {
      this.roleBeginTime = roleBeginTime
    }
    return this
  }

  /**
   * 角色有效结束时间，时间段以为将无法享受角色权限
   * @param {String|Number} roleEndTime
   * @return {RoleChangeDTO}
   */
  setRoleEndTime (roleEndTime) {
    if (roleEndTime) {
      this.roleEndTime = roleEndTime
    }
    return this
  }

  /**
   * 是否固化角色，固化的角色不能修改或必须另外的特殊接口进行修改，防止出错
   * @param {String|Number} solidifyFlag
   * @return {RoleChangeDTO}
   */
  setSolidifyFlag (solidifyFlag) {
    if (solidifyFlag) {
      this.solidifyFlag = solidifyFlag
    }
    return this
  }

  /**
   * 备注
   * @param {String|Number} remark
   * @return {RoleChangeDTO}
   */
  setRemark (remark) {
    if (remark) {
      this.remark = remark
    }
    return this
  }
}

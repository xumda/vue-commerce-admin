export default class PageDTO {
  static instance () {
    return new PageDTO()
  }

  getSize () {
    return this.size
  }

  setSize (size) {
    this.size = size
  }

  setCurrent (current) {
    this.current = current
  }

  getCurrent () {
    return this.current
  }

  /**
   * 条件DTO
   * @return {*}
   */
  getCondition () {
    return this.condition
  }

  setCondition (condition) {
    this.condition = condition
  }
}

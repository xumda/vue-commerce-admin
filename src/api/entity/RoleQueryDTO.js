import OrderByDTO from '@/api/entity/OrderByDTO'

export default class RoleQueryDTO extends OrderByDTO {
  static instance () {
    return new RoleQueryDTO()
  }

  /**
   * 角色编号
   * @param {Array<String|Number>} roleId
   * @return {RoleQueryDTO}
   */
  setRoleId (roleId) {
    if (roleId) {
      this.roleId = roleId
    }
    return this
  }

  /**
   * 角色编号
   * @param {String|Number} roleId
   * @return {RoleQueryDTO}
   */
  addRoleId (roleId) {
    if (roleId) {
      if (this.roleId) {
        this.roleId.push(roleId)
      } else {
        this.roleId = [roleId]
      }
    }
    return this
  }

  /**
   * 角色所属管理类型，STORE 和 ADMIN
   * @param {Array<String|Number>} manageType
   * @return {RoleQueryDTO}
   */
  setManageType (manageType) {
    if (manageType) {
      this.manageType = manageType
    }
    return this
  }

  /**
   * 角色所属管理类型，STORE 和 ADMIN
   * @param {String|Number} manageType
   * @return {RoleQueryDTO}
   */
  addManageType (manageType) {
    if (manageType) {
      if (this.manageType) {
        this.manageType.push(manageType)
      } else {
        this.manageType = [manageType]
      }
    }
    return this
  }

  /**
   * 店铺编号，当 Type=STORE 时有效
   * @param {Array<String|Number>} storeId
   * @return {RoleQueryDTO}
   */
  setStoreId (storeId) {
    if (storeId) {
      this.storeId = storeId
    }
    return this
  }

  /**
   * 店铺编号，当 Type=STORE 时有效
   * @param {String|Number} storeId
   * @return {RoleQueryDTO}
   */
  addStoreId (storeId) {
    if (storeId) {
      if (this.storeId) {
        this.storeId.push(storeId)
      } else {
        this.storeId = [storeId]
      }
    }
    return this
  }

  /**
   * 店铺名称，当 Type=STORE 时有效
   * @param {Array<String|Number>} storeName
   * @return {RoleQueryDTO}
   */
  setStoreName (storeName) {
    if (storeName) {
      this.storeName = storeName
    }
    return this
  }

  /**
   * 店铺名称，当 Type=STORE 时有效
   * @param {String|Number} storeName
   * @return {RoleQueryDTO}
   */
  addStoreName (storeName) {
    if (storeName) {
      if (this.storeName) {
        this.storeName.push(storeName)
      } else {
        this.storeName = [storeName]
      }
    }
    return this
  }

  /**
   * 角色名称
   * @param {Array<String|Number>} roleName
   * @return {RoleQueryDTO}
   */
  setRoleName (roleName) {
    if (roleName) {
      this.roleName = roleName
    }
    return this
  }

  /**
   * 角色名称
   * @param {String|Number} roleName
   * @return {RoleQueryDTO}
   */
  addRoleName (roleName) {
    if (roleName) {
      if (this.roleName) {
        this.roleName.push(roleName)
      } else {
        this.roleName = [roleName]
      }
    }
    return this
  }

  /**
   * 角色有效开始时间，时间段以为将无法享受角色权限
   * @param {Array<String|Number>} roleBeginTimeBegin
   * @return {RoleQueryDTO}
   */
  setRoleBeginTimeBegin (roleBeginTimeBegin) {
    if (roleBeginTimeBegin) {
      this.roleBeginTimeBegin = roleBeginTimeBegin
    }
    return this
  }

  /**
   * 角色有效开始时间，时间段以为将无法享受角色权限
   * @param {Array<String|Number>} roleBeginTimeEnd
   * @return {RoleQueryDTO}
   */
  setRoleBeginTimeEnd (roleBeginTimeEnd) {
    if (roleBeginTimeEnd) {
      this.roleBeginTimeEnd = roleBeginTimeEnd
    }
    return this
  }

  /**
   * 角色有效结束时间，时间段以为将无法享受角色权限
   * @param {Array<String|Number>} roleEndTimeBegin
   * @return {RoleQueryDTO}
   */
  setRoleEndTimeBegin (roleEndTimeBegin) {
    if (roleEndTimeBegin) {
      this.roleEndTimeBegin = roleEndTimeBegin
    }
    return this
  }

  /**
   * 角色有效结束时间，时间段以为将无法享受角色权限
   * @param {Array<String|Number>} roleEndTimeEnd
   * @return {RoleQueryDTO}
   */
  setRoleEndTimeEnd (roleEndTimeEnd) {
    if (roleEndTimeEnd) {
      this.roleEndTimeEnd = roleEndTimeEnd
    }
    return this
  }

  /**
   * 是否是店铺使用角色，如店铺超级管理员角色，普通店铺管理员角色
   * @param {String|Number} storeEmploy
   * @return {RoleQueryDTO}
   */
  setStoreEmploy (storeEmploy) {
    this.storeEmploy = storeEmploy
    return this
  }

  /**
   * 是否固化角色，固化的角色不能修改或必须另外的特殊接口进行修改，防止出错
   * @param {String|Number} solidifyFlag
   * @return {RoleQueryDTO}
   */
  setSolidifyFlag (solidifyFlag) {
    this.solidifyFlag = solidifyFlag
    return this
  }

  /**
   * 创建时间
   * @param {Array<String|Number>} createTimeBegin
   * @return {RoleQueryDTO}
   */
  setCreateTimeBegin (createTimeBegin) {
    if (createTimeBegin) {
      this.createTimeBegin = createTimeBegin
    }
    return this
  }

  /**
   * 创建时间
   * @param {Array<String|Number>} createTimeEnd
   * @return {RoleQueryDTO}
   */
  setCreateTimeEnd (createTimeEnd) {
    if (createTimeEnd) {
      this.createTimeEnd = createTimeEnd
    }
    return this
  }

  /**
   * 创建人员编号
   * @param {Array<String|Number>} createUserId
   * @return {RoleQueryDTO}
   */
  setCreateUserId (createUserId) {
    if (createUserId) {
      this.createUserId = createUserId
    }
    return this
  }

  /**
   * 创建人员编号
   * @param {String|Number} createUserId
   * @return {RoleQueryDTO}
   */
  addCreateUserId (createUserId) {
    if (createUserId) {
      if (this.createUserId) {
        this.createUserId.push(createUserId)
      } else {
        this.createUserId = [createUserId]
      }
    }
    return this
  }

  /**
   * 创建人员类型
   * @param {Array<String|Number>} createUserType
   * @return {RoleQueryDTO}
   */
  setCreateUserType (createUserType) {
    if (createUserType) {
      this.createUserType = createUserType
    }
    return this
  }

  /**
   * 创建人员类型
   * @param {String|Number} createUserType
   * @return {RoleQueryDTO}
   */
  addCreateUserType (createUserType) {
    if (createUserType) {
      if (this.createUserType) {
        this.createUserType.push(createUserType)
      } else {
        this.createUserType = [createUserType]
      }
    }
    return this
  }
}

export default class StoreInfoQueryDTO {
  static instance () {
    return new StoreInfoQueryDTO()
  }

  /**
   * 模糊匹配
   */
  setSearch (val) {
    if (val) {
      this.search = val
    }
  }

  /**
   * 自动增长ID
   */
  setId (val) {
    if (val) {
      this.id = val
    }
  }

  /**
   * 店铺名称，不能重复，需要验重
   */
  setName (val) {
    if (val) {
      this.name = val
    }
  }

  /**
   * 店铺最高权限人员，店主系统账号，唯一一个
   */
  setUserId (val) {
    if (val) {
      this.userId = val
    }
  }

  /**
   * 注册账号名称
   */
  setUserName (val) {
    if (val) {
      this.userName = val
    }
  }

  /**
   * 店铺联系方式1
   */
  setPhone1 (val) {
    if (val) {
      this.phone1 = val
    }
  }

  /**
   * 店铺联系方式2
   */
  setPhone2 (val) {
    if (val) {
      this.phone2 = val
    }
  }

  /**
   * 店铺邮箱联系方式
   */
  setEmail (val) {
    if (val) {
      this.email = val
    }
  }

  /**
   * 店铺微信
   */
  setWeChat (val) {
    if (val) {
      this.weChat = val
    }
  }

  /**
   * 店铺qq
   */
  setQq (val) {
    if (val) {
      this.qq = val
    }
  }

  /**
   * 店铺位置
   */
  setAddress (val) {
    if (val) {
      this.address = val
    }
  }

  /**
   * 营业执照id
   */
  setBusinessId (val) {
    if (val) {
      this.businessId = val
    }
  }

  /**
   * 税务id
   */
  setTaxId (val) {
    if (val) {
      this.taxId = val
    }
  }

  /**
   * 店家详细信息id
   */
  setStoreMasterId (val) {
    if (val) {
      this.storeMasterId = val
    }
  }

  /**
   * 支付方式：支付宝，微信等
   */
  setPayType (val) {
    if (val) {
      this.payType = val
    }
  }

  /**
   * 支付账号：支付宝账号，微信账号等
   */
  setPayAccount (val) {
    if (val) {
      this.payAccount = val
    }
  }

  /**
   * 押金
   */
  setMoneyBegin (val) {
    if (val) {
      this.moneyBegin = val
    }
  }

  /**
   * 押金
   */
  setMoneyEnd (val) {
    if (val) {
      this.moneyEnd = val
    }
  }

  /**
   * 店铺官网，非系统提供页面
   */
  setWebsite (val) {
    if (val) {
      this.website = val
    }
  }

  /**
   * 创建店铺时间
   */
  setCreateTimeBegin (val) {
    if (val) {
      this.createTimeBegin = val
    }
  }

  /**
   * 创建店铺时间
   */
  setCreateTimeEnd (val) {
    if (val) {
      this.createTimeEnd = val
    }
  }

  /**
   * 更新店铺时间
   */
  setUpdateTimeBegin (val) {
    if (val) {
      this.updateTimeBegin = val
    }
  }

  /**
   * 更新店铺时间
   */
  setUpdateTimeEnd (val) {
    if (val) {
      this.updateTimeEnd = val
    }
  }

  /**
   * 删除店铺时间
   */
  setDeleteTimeBegin (val) {
    if (val) {
      this.deleteTimeBegin = val
    }
  }

  /**
   * 删除店铺时间
   */
  setDeleteTimeEnd (val) {
    if (val) {
      this.deleteTimeEnd = val
    }
  }

  /**
   * 店铺状态：待审核，审核中，审核失败，正常，歇业中，禁止运营（随机审核不通过，押金不足等）
   */
  setState (val) {
    if (val) {
      this.state = val
    }
  }

  /**
   * 状态描述，可以是审核失败原因
   */
  setStateDesc (val) {
    if (val) {
      this.stateDesc = val
    }
  }

  /**
   * 审核人员
   */
  setCheckUserId (val) {
    if (val) {
      this.checkUserId = val
    }
  }

  /**
   * 审核人员名称
   */
  setCheckUserName (val) {
    if (val) {
      this.checkUserName = val
    }
  }

  /**
   * 审核时间
   */
  setCheckTimeBegin (val) {
    if (val) {
      this.checkTimeBegin = val
    }
  }

  /**
   * 审核时间
   */
  setCheckTimeEnd (val) {
    if (val) {
      this.checkTimeEnd = val
    }
  }

  /**
   * 备注信息
   */
  setRemark (val) {
    if (val) {
      this.remark = val
    }
  }
}

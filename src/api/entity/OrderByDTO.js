import SortCode from '@/constants/SortCode'

export default class OrderByDTO {
  constructor () {
    this.setOrderByList([])
  }

  /**
   * 设置
   * @param {Array} orderByList
   */
  setOrderByList (orderByList) {
    this.orderByList = orderByList
  }

  /**
   * 获取
   * @return {Array}
   */
  getOrderByList () {
    return this.orderByList
  }

  /**
   * 添加一个排序规则
   * @param {String} column 排序字段
   * @param {String} order 排序方式，见SortCode
   */
  addOrder (column, order) {
    if (!this.orderByList) {
      this.setOrderByList([])
    }
    this.orderByList.push({
      column: column,
      order: order
    })
  }

  /**
   * 添加一个正序排序规则
   * @param column 排序字段
   */
  addOrderAsc (column) {
    this.addOrder(column, SortCode.SORT_ASC)
  }

  /**
   * 添加一个倒序排序规则
   * @param column 排序字段
   */
  addOrderDesc (column) {
    this.addOrder(column, SortCode.SORT_DESC)
  }
}

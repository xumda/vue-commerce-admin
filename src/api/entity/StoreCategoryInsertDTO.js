export default class StoreCategoryInsertDTO {
  static instance () {
    return new StoreCategoryInsertDTO()
  }

  setParentId (parentId) {
    if (parentId) {
      this.parentId = parentId
    }
  }

  setName (name) {
    if (name) {
      this.name = name
    }
  }

  setDescription (description) {
    if (description) {
      this.description = description
    }
  }

  setRemark (remark) {
    if (remark) {
      this.remark = remark
    }
  }
}

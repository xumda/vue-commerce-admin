export default class GoodsStoreQueryDTO {
  static instance () {
    const dto = new GoodsStoreQueryDTO()
    dto.setFilter({})
    dto.setSort([])
    return dto
  }

  getSearch () {
    return this.search
  }

  setSearch (search) {
    this.search = search
  }

  /**
   * 设置筛选条件
   * @param {Object} filter 筛选条件
   */
  setFilter (filter) {
    this.filter = filter
  }

  getFilter () {
    return this.filter
  }

  addFilter (column, value) {
    this.filter[column] = value
  }

  setSort (sort) {
    this.sort = sort
  }

  getSort () {
    return this.sort
  }

  addSort (column, order) {
    this.sort.push({
      column: column,
      order: order
    })
  }

  addSortAsc (column) {
    this.sort.push({
      column: column,
      order: GoodsStoreQueryConstant.SORT_ASC
    })
  }

  addSortDesc (column) {
    this.sort.push({
      column: column,
      order: GoodsStoreQueryConstant.SORT_DESC
    })
  }
}

export const GoodsStoreQueryConstant = {

  FILTER_COLUMN_GOOD_CATEGORY: 'category',
  FILTER_COLUMN_GROUP_NAME: 'group',
  FILTER_COLUMN_SELL_POINT: 'sellPoint',
  FILTER_COLUMN_STATE: 'state',

  SORT_COLUMN_NAME: 'name',
  SORT_COLUMN_CATEGORY: 'goodCategoryName',
  SORT_COLUMN_GROUP: 'groupName',
  SORT_COLUMN_SELL_POINT: 'sellPoint',
  SORT_COLUMN_FREIGHT: 'freight',
  SORT_COLUMN_CREATE_TIME: 'createTime',
  SORT_COLUMN_REMARK: 'remark',
  SORT_COLUMN_STATE: 'state',

  SORT_ASC: 'ASC',
  SORT_DESC: 'DESC'

}

export default class PermissionChangeDTO {
  static instance () {
    return new PermissionChangeDTO()
  }

  /**
   * 权限Id
   * @param {String|Number} permissionId
   * @return {PermissionChangeDTO}
   */
  setPermissionId (permissionId) {
    if (permissionId) {
      this.permissionId = permissionId
    }
    return this
  }

  /**
   * 权限编号，真正的权限判定标志，不能重复
   * @param {String} permissionCode
   * @return {PermissionChangeDTO}
   */
  setPermissionCode (permissionCode) {
    if (permissionCode) {
      this.permissionCode = permissionCode
    }
    return this
  }

  /**
   * 权限名称，只是用来查看
   * @param {String} permissionName
   * @return {PermissionChangeDTO}
   */
  setPermissionName (permissionName) {
    if (permissionName) {
      this.permissionName = permissionName
    }
    return this
  }

  /**
   * 权限描述
   * @param {String} permissionDesc
   * @return {PermissionChangeDTO}
   */
  setPermissionDesc (permissionDesc) {
    if (permissionDesc) {
      this.permissionDesc = permissionDesc
    }
    return this
  }
}

export default class PermissionInsertDTO {
  static instance () {
    return new PermissionInsertDTO()
  }

  /**
   * 权限编号，真正的权限判定标志，不能重复
   * @param {String} permissionCode
   * @return {PermissionInsertDTO}
   */
  setPermissionCode (permissionCode) {
    if (permissionCode) {
      this.permissionCode = permissionCode
    }
    return this
  }

  /**
   * 权限名称，只是用来查看
   * @param {String} permissionName
   * @return {PermissionInsertDTO}
   */
  setPermissionName (permissionName) {
    if (permissionName) {
      this.permissionName = permissionName
    }
    return this
  }

  /**
   * 权限描述
   * @param {String} permissionDesc
   * @return {PermissionInsertDTO}
   */
  setPermissionDesc (permissionDesc) {
    if (permissionDesc) {
      this.permissionDesc = permissionDesc
    }
    return this
  }
}

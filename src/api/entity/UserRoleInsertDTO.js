export default class UserRoleInsertDTO {
  static instance () {
    return new UserRoleInsertDTO()
  }

  /**
   * 用户角色有效期开始时间
   * @param {String|Number} beginTime
   * @return {UserRoleInsertDTO}
   */
  setBeginTime (beginTime) {
    if (beginTime) {
      this.beginTime = beginTime
    }
    return this
  }

  /**
   * 用户角色有效期结束时间
   * @param {String|Number} endTime
   * @return {UserRoleInsertDTO}
   */
  setEndTime (endTime) {
    if (endTime) {
      this.endTime = endTime
    }
    return this
  }

  /**
   * 用户编号
   * @param {Array<String|Number>} accountList
   * @return {UserRoleInsertDTO}
   */
  setAccountList (accountList) {
    if (accountList) {
      this.accountList = accountList
    }
    return this
  }

  /**
   * 用户编号
   * @param {String|Number} accountList
   * @return {UserRoleInsertDTO}
   */
  addAccountList (accountList) {
    if (accountList) {
      if (this.accountList) {
        this.accountList.push(accountList)
      } else {
        this.accountList = [accountList]
      }
    }
    return this
  }

  /**
   * 角色编号
   * @param {Array<String|Number>} roleIds
   * @return {UserRoleInsertDTO}
   */
  setRoleIds (roleIds) {
    if (roleIds) {
      this.roleIds = roleIds
    }
    return this
  }

  /**
   * 用户编号
   * @param {String|Number} roleIds
   * @return {UserRoleInsertDTO}
   */
  addRoleIds (roleIds) {
    if (roleIds) {
      if (this.accountList) {
        this.roleIds.push(roleIds)
      } else {
        this.roleIds = [roleIds]
      }
    }
    return this
  }
}

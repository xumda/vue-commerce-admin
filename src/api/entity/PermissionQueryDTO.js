import OrderByDTO from '@/api/entity/OrderByDTO'

export default class PermissionQueryDTO extends OrderByDTO {
  static instance () {
    return new PermissionQueryDTO()
  }

  /**
   * 搜索字段
   * @param {Array<String|Number>} search
   * @return {PermissionQueryDTO}
   */
  setSearch (search) {
    if (search) {
      this.search = search
    }
    return this
  }

  /**
   * 搜索字段
   * @param {String|Number} search
   * @return {PermissionQueryDTO}
   */
  addSearch (search) {
    if (search) {
      if (this.search) {
        this.search.push(search)
      } else {
        this.search = [search]
      }
    }
    return this
  }

  /**
   * 分配编号，唯一主键
   * @param {Array<String|Number>} permissionId
   * @return {PermissionQueryDTO}
   */
  setPermissionId (permissionId) {
    if (permissionId) {
      this.permissionId = permissionId
    }
    return this
  }

  /**
   * 分配编号，唯一主键
   * @param {String|Number} permissionId
   * @return {PermissionQueryDTO}
   */
  addPermissionId (permissionId) {
    if (permissionId) {
      if (this.permissionId) {
        this.permissionId.push(permissionId)
      } else {
        this.permissionId = [permissionId]
      }
    }
    return this
  }

  /**
   * 权限编号，真正的权限判定标志，不能重复
   * @param {Array<String|Number>} permissionCode
   * @return {PermissionQueryDTO}
   */
  setPermissionCode (permissionCode) {
    if (permissionCode) {
      this.permissionCode = permissionCode
    }
    return this
  }

  /**
   * 权限编号，真正的权限判定标志，不能重复
   * @param {String|Number} permissionCode
   * @return {PermissionQueryDTO}
   */
  addPermissionCode (permissionCode) {
    if (permissionCode) {
      if (this.permissionCode) {
        this.permissionCode.push(permissionCode)
      } else {
        this.permissionCode = [permissionCode]
      }
    }
    return this
  }

  /**
   * 权限名称，只是用来查看
   * @param {Array<String|Number>} permissionName
   * @return {PermissionQueryDTO}
   */
  setPermissionName (permissionName) {
    if (permissionName) {
      this.permissionName = permissionName
    }
    return this
  }

  /**
   * 权限名称，只是用来查看
   * @param {String|Number} permissionName
   * @return {PermissionQueryDTO}
   */
  addPermissionName (permissionName) {
    if (permissionName) {
      if (this.permissionName) {
        this.permissionName.push(permissionName)
      } else {
        this.permissionName = [permissionName]
      }
    }
    return this
  }

  /**
   * 创建模式，自动创建和手动创建
   * @param {Array<String|Number>} createMode
   * @return {PermissionQueryDTO}
   */
  setCreateMode (createMode) {
    if (createMode) {
      this.createMode = createMode
    }
    return this
  }

  /**
   * 创建模式，自动创建和手动创建
   * @param {String|Number} createMode
   * @return {PermissionQueryDTO}
   */
  addCreateMode (createMode) {
    if (createMode) {
      if (this.createMode) {
        this.createMode.push(createMode)
      } else {
        this.createMode = [createMode]
      }
    }
    return this
  }

  /**
   * 创建时间
   * @param {String} createTimeBegin
   * @return {PermissionQueryDTO}
   */
  setCreateTimeBegin (createTimeBegin) {
    if (createTimeBegin) {
      this.createTimeBegin = createTimeBegin
    }
    return this
  }

  /**
   * 创建时间
   * @param {String} createTimeEnd
   * @return {PermissionQueryDTO}
   */
  setCreateTimeEnd (createTimeEnd) {
    if (createTimeEnd) {
      this.createTimeEnd = createTimeEnd
    }
    return this
  }

  /**
   * 创建人员编号
   * @param {Array<String|Number>} createUserId
   * @return {PermissionQueryDTO}
   */
  setCreateUserId (createUserId) {
    if (createUserId) {
      this.createUserId = createUserId
    }
    return this
  }

  /**
   * 创建人员编号
   * @param {String|Number} createUserId
   * @return {PermissionQueryDTO}
   */
  addCreateUserId (createUserId) {
    if (createUserId) {
      if (this.createUserId) {
        this.createUserId.push(createUserId)
      } else {
        this.createUserId = [createUserId]
      }
    }
    return this
  }

  /**
   * 创建人员类型
   * @param {Array<String|Number>} createUserType
   * @return {PermissionQueryDTO}
   */
  setCreateUserType (createUserType) {
    if (createUserType) {
      this.createUserType = createUserType
    }
    return this
  }

  /**
   * 创建人员类型
   * @param {String|Number} createUserType
   * @return {PermissionQueryDTO}
   */
  addCreateUserType (createUserType) {
    if (createUserType) {
      if (this.createUserType) {
        this.createUserType.push(createUserType)
      } else {
        this.createUserType = [createUserType]
      }
    }
    return this
  }
}

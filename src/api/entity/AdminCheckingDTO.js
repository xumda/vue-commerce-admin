export default class AdminCheckingDTO {
  static instance () {
    return new AdminCheckingDTO()
  }

  setId (id) {
    this.id = id
  }

  setSuccess (success) {
    this.success = success
  }

  setMessage (message) {
    this.message = message
  }
}

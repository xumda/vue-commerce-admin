export default class SystemMenuInsertDTO {
  static instance () {
    return new SystemMenuInsertDTO()
  }

  /**
   * 菜单名称
   * @param {String|Number} menuName
   * @return {SystemMenuInsertDTO}
   */
  setMenuName (menuName) {
    if (menuName) {
      this.menuName = menuName
    }
    return this
  }

  /**
   * 菜单图标
   * @param {String|Number} menuIcon
   * @return {SystemMenuInsertDTO}
   */
  setMenuIcon (menuIcon) {
    if (menuIcon) {
      this.menuIcon = menuIcon
    }
    return this
  }

  /**
   * 菜单编码
   * @param {String|Number} menuCode
   * @return {SystemMenuInsertDTO}
   */
  setMenuCode (menuCode) {
    if (menuCode) {
      this.menuCode = menuCode
    }
    return this
  }

  /**
   * 菜单链接路径
   * @param {String|Number} menuUrl
   * @return {SystemMenuInsertDTO}
   */
  setMenuUrl (menuUrl) {
    if (menuUrl) {
      this.menuUrl = menuUrl
    }
    return this
  }

  /**
   * 排序字段，小数在前
   * @param {String|Number} menuOrder
   * @return {SystemMenuInsertDTO}
   */
  setMenuOrder (menuOrder) {
    if (menuOrder) {
      this.menuOrder = menuOrder
    }
    return this
  }

  /**
   * 菜单描述
   * @param {String|Number} menuDesc
   * @return {SystemMenuInsertDTO}
   */
  setMenuDesc (menuDesc) {
    if (menuDesc) {
      this.menuDesc = menuDesc
    }
    return this
  }

  /**
   * 父节点编号，第一级节点为子系统短码
   * @param {String|Number} parentMenuId
   * @return {SystemMenuInsertDTO}
   */
  setParentMenuId (parentMenuId) {
    if (parentMenuId) {
      this.parentMenuId = parentMenuId
    }
    return this
  }
}

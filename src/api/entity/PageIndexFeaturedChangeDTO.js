export default class PageIndexFeaturedChangeDTO {
  static instance () {
    return new PageIndexFeaturedChangeDTO()
  }

  /**
   * 设置 id
   * @param {String|Number} id
   */
  setId (id) {
    if (id) {
      this.id = id
    }
  }

  /**
   * 设置 title
   * @param {String} title
   */
  setTitle (title) {
    if (title) {
      this.title = title
    }
  }

  /**
   * 设置 subTitle
   * @param {String} subTitle
   */
  setSubTitle (subTitle) {
    if (subTitle) {
      this.subTitle = subTitle
    }
  }

  /**
   * 设置 image
   * @param {String} image
   */
  setImage (image) {
    if (image) {
      this.image = image
    }
  }

  /**
   * 设置 imageFileId
   * @param {String|Number} imageFileId
   */
  setImageFileId (imageFileId) {
    if (imageFileId) {
      this.imageFileId = imageFileId
    }
  }

  /**
   * 设置 linkGoodId
   * @param {String|Number} linkGoodId
   */
  setLinkGoodId (linkGoodId) {
    if (linkGoodId) {
      this.linkGoodId = linkGoodId
    }
  }

  /**
   * 设置 direction
   * @param {String} direction
   */
  setDirection (direction) {
    if (direction) {
      this.direction = direction
    }
  }

  /**
   * 设置 position
   * @param {String} position
   */
  setPosition (position) {
    if (position) {
      this.position = position
    }
  }
}

/**
 * type参数选择项
 * @type {{TYPE_PICTURE: string, TYPE_DEFAULT: string}}
 */
export const PageIndexFeaturedDirection = {
  DIRECTION_LEFT: 'LEFT',
  DIRECTION_RIGHT: 'RIGHT'
}

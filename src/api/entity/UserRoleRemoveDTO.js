export default class UserRoleRemoveDTO {
  static instance () {
    return new UserRoleRemoveDTO()
  }

  /**
   * 用户编号
   * @param {String|Number} account
   * @return {UserRoleRemoveDTO}
   */
  setAccount (account) {
    if (account) {
      this.account = account
    }
    return this
  }

  /**
   * 角色编号
   * @param {String|Number} roleId
   * @return {UserRoleRemoveDTO}
   */
  setRoleId (roleId) {
    if (roleId) {
      this.roleId = roleId
    }
    return this
  }
}

export default class PageIndexCarouselChangeDTO {
  static instance () {
    return new PageIndexCarouselChangeDTO()
  }

  /**
   * 设置 id
   * @param {String|Number} id
   */
  setId (id) {
    if (id) {
      this.id = id
    }
  }

  /**
   * 设置 type
   * @param {String} type
   */
  setType (type) {
    if (type) {
      this.type = type
    }
  }

  /**
   * 设置 title
   * @param {String} title
   */
  setTitle (title) {
    if (title) {
      this.title = title
    }
  }

  /**
   * 设置 sequence
   * @param {String|Number} sequence
   */
  setSequence (sequence) {
    if (sequence) {
      this.sequence = sequence
    }
  }

  /**
   * 设置 image
   * @param {String} image
   */
  setImage (image) {
    if (image) {
      this.image = image
    }
  }

  /**
   * 设置 imageFileId
   * @param {String|Number} imageFileId
   */
  setImageFileId (imageFileId) {
    if (imageFileId) {
      this.imageFileId = imageFileId
    }
  }

  /**
   * 设置 backImage
   * @param {String} backImage
   */
  setBackImage (backImage) {
    if (backImage) {
      this.backImage = backImage
    }
  }

  /**
   * 设置 backImageFileId
   * @param {String|Number} backImageFileId
   */
  setBackImageFileId (backImageFileId) {
    if (backImageFileId) {
      this.backImageFileId = backImageFileId
    }
  }

  /**
   * 设置 linkGoodId
   * @param {String|Number} linkGoodId
   */
  setLinkGoodId (linkGoodId) {
    if (linkGoodId) {
      this.linkGoodId = linkGoodId
    }
  }

  /**
   * 设置 direction
   * @param {String} direction
   */
  setDirection (direction) {
    if (direction) {
      this.direction = direction
    }
  }

  /**
   * 设置 goodIdList
   * @param {Array<Number|String>} goodIdList
   */
  setGoodIdList (goodIdList) {
    if (goodIdList) {
      this.goodIdList = goodIdList
    }
  }

  /**
   * 添加一个 goodIdList
   * @param {Number|String} goodIdList
   */
  addGoodIdList (goodIdList) {
    if (goodIdList) {
      if (this.goodIdList && typeof this.goodIdList === 'object') {
        this.goodIdList.push(goodIdList)
      } else {
        this.goodIdList = [goodIdList]
      }
    }
  }
}

/**
 * type参数选择项
 * @type {{TYPE_PICTURE: string, TYPE_DEFAULT: string}}
 */
export const PageIndexCarouselType = {
  TYPE_DEFAULT: 'DEFAULT',
  TYPE_PICTURE: 'PICTURE'
}

/**
 * type参数选择项
 * @type {{TYPE_PICTURE: string, TYPE_DEFAULT: string}}
 */
export const PageIndexCarouselDirection = {
  DIRECTION_LEFT: 'LEFT',
  DIRECTION_RIGHT: 'RIGHT'
}

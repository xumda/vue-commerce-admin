export default class GoodsInsertDTO {
  static instance () {
    return new GoodsInsertDTO()
  }

  getGroupId () {
    return this.groupId
  }

  getName () {
    return this.name
  }

  getSimpleDesc () {
    return this.simpleDesc
  }

  getDescription () {
    return this.description
  }

  getGoodCategoryId () {
    return this.goodCategoryId
  }

  getSellPoint () {
    return this.sellPoint
  }

  getFreight () {
    return this.freight
  }

  getRemark () {
    return this.remark
  }

  /**
   * @return {Array<SubGoodInsertDTO>}
   */
  getSubGoodList () {
    return this.subGoodList
  }

  setGroupId (groupId) {
    this.groupId = groupId
  }

  setName (name) {
    this.name = name
  }

  setSimpleDesc (simpleDesc) {
    this.simpleDesc = simpleDesc
  }

  setDescription (description) {
    this.description = description
  }

  setGoodCategoryId (goodCategoryId) {
    this.goodCategoryId = goodCategoryId
  }

  setSellPoint (sellPoint) {
    this.sellPoint = sellPoint
  }

  setFreight (freight) {
    this.freight = freight
  }

  setRemark (remark) {
    this.remark = remark
  }

  setSubGoodList (subGoodList) {
    this.subGoodList = subGoodList
  }
}

/**
 * 条件查询商品数据信息，包括分页
 */
export default class GoodDetailQueryDTO {
  static instance () {
    return new GoodDetailQueryDTO()
  }

  /**
   * 设置 searchType
   * @param {String} searchType
   */
  setSearchType (searchType) {
    if (searchType) {
      this.searchType = searchType
    }
  }

  /**
   * 设置 search
   * @param {Array<String>} search
   */
  setSearch (search) {
    if (search) {
      this.search = search
    }
  }

  /**
   * 添加 search
   * @param {String} search
   */
  addSearch (search) {
    if (search) {
      if (this.search) {
        this.search.push(search)
      } else {
        this.search = [search]
      }
    }
  }

  /**
   * 设置 goodId
   * @param {Array<Number>} goodId
   */
  setGoodId (goodId) {
    if (goodId) {
      this.goodId = goodId
    }
  }

  /**
   * 添加 goodId
   * @param {Number} goodId
   */
  addGoodId (goodId) {
    if (goodId) {
      if (this.goodId) {
        this.goodId.push(goodId)
      } else {
        this.goodId = [goodId]
      }
    }
  }

  /**
   * 设置 groupId
   * @param {Array<Number>} groupId
   */
  setGroupId (groupId) {
    if (groupId) {
      this.groupId = groupId
    }
  }

  /**
   * 添加 groupId
   * @param {Number} groupId
   */
  addGroupId (groupId) {
    if (groupId) {
      if (this.groupId) {
        this.groupId.push(groupId)
      } else {
        this.groupId = [groupId]
      }
    }
  }

  /**
   * 设置 groupName
   * @param {Array<String>} groupName
   */
  setGroupName (groupName) {
    if (groupName) {
      this.groupName = groupName
    }
  }

  /**
   * 添加 groupName
   * @param {String} groupName
   */
  addGroupName (groupName) {
    if (groupName) {
      if (this.groupName) {
        this.groupName.push(groupName)
      } else {
        this.groupName = [groupName]
      }
    }
  }

  /**
   * 设置 storeId
   * @param {Array<Number>} storeId
   */
  setStoreId (storeId) {
    if (storeId) {
      this.storeId = storeId
    }
  }

  /**
   * 添加 storeId
   * @param {Number} storeId
   */
  addStoreId (storeId) {
    if (storeId) {
      if (this.storeId) {
        this.storeId.push(storeId)
      } else {
        this.storeId = [storeId]
      }
    }
  }

  /**
   * 设置 storeName
   * @param {Array<String>} storeName
   */
  setStoreName (storeName) {
    if (storeName) {
      this.storeName = storeName
    }
  }

  /**
   * 添加 storeName
   * @param {String} storeName
   */
  addStoreName (storeName) {
    if (storeName) {
      if (this.storeName) {
        this.storeName.push(storeName)
      } else {
        this.storeName = [storeName]
      }
    }
  }

  /**
   * 设置 goodCategoryId
   * @param {Array<Number>} goodCategoryId
   */
  setGoodCategoryId (goodCategoryId) {
    if (goodCategoryId) {
      this.goodCategoryId = goodCategoryId
    }
  }

  /**
   * 添加 goodCategoryId
   * @param {Number} goodCategoryId
   */
  addGoodCategoryId (goodCategoryId) {
    if (goodCategoryId) {
      if (this.goodCategoryId) {
        this.goodCategoryId.push(goodCategoryId)
      } else {
        this.goodCategoryId = [goodCategoryId]
      }
    }
  }

  /**
   * 设置 goodName
   * @param {Array<String>} goodName
   */
  setGoodName (goodName) {
    if (goodName) {
      this.goodName = goodName
    }
  }

  /**
   * 添加 goodName
   * @param {String} goodName
   */
  addGoodName (goodName) {
    if (goodName) {
      if (this.goodName) {
        this.goodName.push(goodName)
      } else {
        this.goodName = [goodName]
      }
    }
  }

  /**
   * 设置 createGoodUserId
   * @param {Array<Number>} createGoodUserId
   */
  setCreateGoodUserId (createGoodUserId) {
    if (createGoodUserId) {
      this.createGoodUserId = createGoodUserId
    }
  }

  /**
   * 添加 createGoodUserId
   * @param {Number} createGoodUserId
   */
  addCreateGoodUserId (createGoodUserId) {
    if (createGoodUserId) {
      if (this.createGoodUserId) {
        this.createGoodUserId.push(createGoodUserId)
      } else {
        this.createGoodUserId = [createGoodUserId]
      }
    }
  }

  /**
   * 设置 checkGoodUserId
   * @param {Array<Number>} checkGoodUserId
   */
  setCheckGoodUserId (checkGoodUserId) {
    if (checkGoodUserId) {
      this.checkGoodUserId = checkGoodUserId
    }
  }

  /**
   * 添加 checkGoodUserId
   * @param {Number} checkGoodUserId
   */
  addCheckGoodUserId (checkGoodUserId) {
    if (checkGoodUserId) {
      if (this.checkGoodUserId) {
        this.checkGoodUserId.push(checkGoodUserId)
      } else {
        this.checkGoodUserId = [checkGoodUserId]
      }
    }
  }

  /**
   * 设置 goodState
   * @param {Array<String>} goodState
   */
  setGoodState (goodState) {
    if (goodState) {
      this.goodState = goodState
    }
  }

  /**
   * 添加 goodState
   * @param {String} goodState
   */
  addGoodState (goodState) {
    if (goodState) {
      if (this.goodState) {
        this.goodState.push(goodState)
      } else {
        this.goodState = [goodState]
      }
    }
  }

  /**
   * 设置 createGoodTimeBegin
   * @param {String} createGoodTimeBegin
   */
  setCreateGoodTimeBegin (createGoodTimeBegin) {
    if (createGoodTimeBegin) {
      this.createGoodTimeBegin = createGoodTimeBegin
    }
  }

  /**
   * 设置 createGoodTimeEnd
   * @param {String} createGoodTimeEnd
   */
  setCreateGoodTimeEnd (createGoodTimeEnd) {
    if (createGoodTimeEnd) {
      this.createGoodTimeEnd = createGoodTimeEnd
    }
  }

  /**
   * 设置 checkGoodTimeBegin
   * @param {String} checkGoodTimeBegin
   */
  setCheckGoodTimeBegin (checkGoodTimeBegin) {
    if (checkGoodTimeBegin) {
      this.checkGoodTimeBegin = checkGoodTimeBegin
    }
  }

  /**
   * 设置 checkGoodTimeEnd
   * @param {String} checkGoodTimeEnd
   */
  setCheckGoodTimeEnd (checkGoodTimeEnd) {
    if (checkGoodTimeEnd) {
      this.checkGoodTimeEnd = checkGoodTimeEnd
    }
  }

  /**
   * 设置 pushGoodTimeBegin
   * @param {String} pushGoodTimeBegin
   */
  setPushGoodTimeBegin (pushGoodTimeBegin) {
    if (pushGoodTimeBegin) {
      this.pushGoodTimeBegin = pushGoodTimeBegin
    }
  }

  /**
   * 设置 pushGoodTimeEnd
   * @param {String} pushGoodTimeEnd
   */
  setPushGoodTimeEnd (pushGoodTimeEnd) {
    if (pushGoodTimeEnd) {
      this.pushGoodTimeEnd = pushGoodTimeEnd
    }
  }

  /**
   * 设置 minPriceBegin
   * @param {String|Number} minPriceBegin
   */
  setMinPriceBegin (minPriceBegin) {
    if (minPriceBegin) {
      this.minPriceBegin = minPriceBegin
    }
  }

  /**
   * 设置 minPriceEnd
   * @param {String|Number} minPriceEnd
   */
  setMinPriceEnd (minPriceEnd) {
    if (minPriceEnd) {
      this.minPriceEnd = minPriceEnd
    }
  }

  /**
   * 设置 maxPriceBegin
   * @param {String|Number} maxPriceBegin
   */
  setMaxPriceBegin (maxPriceBegin) {
    if (maxPriceBegin) {
      this.maxPriceBegin = maxPriceBegin
    }
  }

  /**
   * 设置 maxPriceEnd
   * @param {String|Number} maxPriceEnd
   */
  setMaxPriceEnd (maxPriceEnd) {
    if (maxPriceEnd) {
      this.maxPriceEnd = maxPriceEnd
    }
  }

  /**
   * 设置 goodScoreBegin
   * @param {String|Number} goodScoreBegin
   */
  setGoodScoreBegin (goodScoreBegin) {
    if (goodScoreBegin) {
      this.goodScoreBegin = goodScoreBegin
    }
  }

  /**
   * 设置 goodScoreEnd
   * @param {String|Number} goodScoreEnd
   */
  setGoodScoreEnd (goodScoreEnd) {
    if (goodScoreEnd) {
      this.goodScoreEnd = goodScoreEnd
    }
  }
}

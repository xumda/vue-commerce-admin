export default class MessageInsertDTO {
  static instance () {
    return new MessageInsertDTO()
  }

  setUserId (userId) {
    if (userId) {
      this.userId = userId
    }
  }

  setUserType (userType) {
    if (userType) {
      this.userType = userType
    }
  }

  setUserName (userName) {
    if (userName) {
      this.userName = userName
    }
  }

  setMessageType (messageType) {
    if (messageType) {
      this.messageType = messageType
    }
  }

  setMessageTitle (messageTitle) {
    if (messageTitle) {
      this.messageTitle = messageTitle
    }
  }

  setMessageUrl (messageUrl) {
    if (messageUrl) {
      this.messageUrl = messageUrl
    }
  }

  setMessageContent (messageContent) {
    if (messageContent) {
      this.messageContent = messageContent
    }
  }
}

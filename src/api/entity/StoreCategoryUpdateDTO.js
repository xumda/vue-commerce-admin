export default class StoreCategoryUpdateDTO {
  static instance () {
    return new StoreCategoryUpdateDTO()
  }

  setId (id) {
    if (id) {
      this.id = id
    }
  }

  setName (name) {
    if (name) {
      this.name = name
    }
  }

  setDescription (description) {
    if (description) {
      this.description = description
    }
  }

  setRemark (remark) {
    if (remark) {
      this.remark = remark
    }
  }
}

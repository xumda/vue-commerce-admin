export default class RoleInsertAdminDTO {
  static instance () {
    return new RoleInsertAdminDTO()
  }

  /**
   * 角色名称
   * @param {String|Number} roleName
   * @return {RoleInsertAdminDTO}
   */
  setRoleName (roleName) {
    if (roleName) {
      this.roleName = roleName
    }
    return this
  }

  /**
   * 角色描述
   * @param {String|Number} roleDesc
   * @return {RoleInsertAdminDTO}
   */
  setRoleDesc (roleDesc) {
    if (roleDesc) {
      this.roleDesc = roleDesc
    }
    return this
  }

  /**
   * 角色有效开始时间，时间段以为将无法享受角色权限
   * @param {String|Number} roleBeginTime
   * @return {RoleInsertAdminDTO}
   */
  setRoleBeginTime (roleBeginTime) {
    if (roleBeginTime) {
      this.roleBeginTime = roleBeginTime
    }
    return this
  }

  /**
   * 角色有效结束时间，时间段以为将无法享受角色权限
   * @param {String|Number} roleEndTime
   * @return {RoleInsertAdminDTO}
   */
  setRoleEndTime (roleEndTime) {
    if (roleEndTime) {
      this.roleEndTime = roleEndTime
    }
    return this
  }

  /**
   * 是否是店铺使用角色，如店铺超级管理员角色，普通店铺管理员角色
   * @param {String|Number} storeEmploy
   * @return {RoleInsertAdminDTO}
   */
  setStoreEmploy (storeEmploy) {
    if (storeEmploy) {
      this.storeEmploy = storeEmploy
    }
    return this
  }

  /**
   * 是否固化角色，固化的角色不能修改或必须另外的特殊接口进行修改，防止出错
   * @param {String|Number} solidifyFlag
   * @return {RoleInsertAdminDTO}
   */
  setSolidifyFlag (solidifyFlag) {
    if (solidifyFlag) {
      this.solidifyFlag = solidifyFlag
    }
    return this
  }

  /**
   * 备注
   * @param {String|Number} remark
   * @return {RoleInsertAdminDTO}
   */
  setRemark (remark) {
    if (remark) {
      this.remark = remark
    }
    return this
  }
}

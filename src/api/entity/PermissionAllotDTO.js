export default class PermissionAllotDTO {
  static instance () {
    return new PermissionAllotDTO()
  }

  /**
   * 分配对象，用户，角色
   * @param {String|Number} allotPart
   * @return {PermissionAllotDTO}
   */
  setAllotPart (allotPart) {
    if (allotPart) {
      this.allotPart = allotPart
    }
    return this
  }

  /**
   * 账户编号或角色编号
   * @param {String|Number} partId
   * @return {PermissionAllotDTO}
   */
  setPartId (partId) {
    if (partId) {
      this.partId = partId
    }
    return this
  }

  /**
   * 权限列表
   * @param {Array<String|Number>} permissionIds
   * @return {PermissionAllotDTO}
   */
  setPermissionIds (permissionIds) {
    if (permissionIds) {
      this.permissionIds = permissionIds
    }
    return this
  }

  /**
   * 权限列表
   * @param {String|Number} permissionIds
   * @return {PermissionAllotDTO}
   */
  addPermissionIds (permissionIds) {
    if (permissionIds) {
      if (this.permissionIds) {
        this.permissionIds.push(permissionIds)
      } else {
        this.permissionIds = [permissionIds]
      }
    }
    return this
  }
}

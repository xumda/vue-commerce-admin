/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
import { fallbackRequestGet } from '@/utils/Request'

const URL_PREFIX = 'middle-goods/'

/**
 * 获取商品类别信息，结果表示为一棵树
 * @param {Function} successCallback 成功回调，参数为data
 * @param {Function} fullCallback 肯定触发的回调，先于callback
 */
export const queryGoodGroupTree = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'good_group/queryTree', {}, {}, true, data => {
    if (successCallback) {
      successCallback(data)
    }
  }, null, fullCallback)
}

import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 添加多个菜单目录
 * @param {Array<SystemMenuInsertDTO>} list 新添加的菜单目录数据列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否添加成功
 */
export const insertMenu = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'system_menu/insertMenu', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 更新多个菜单目录
 * @param {Array<SystemMenuChangeDTO>} list 需要更新的菜单目录数据列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否更新成功
 */
export const updateMenuById = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'system_menu/updateMenuById', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据id删除菜单目录，删除包括子节点的所有节点
 * @param {Number|String} id 菜单目录id
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否更新成功
 */
export const removeMenuAllById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'system_menu/removeMenuAllById', { id }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据id删除菜单目录，删除包括子节点的所有节点
 * @param {Number|String} ids 菜单目录id
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否更新成功
 */
export const removeMenuAllByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'system_menu/removeMenuAllByIds', ids, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据id获取菜单目录数据
 * @param {Number|String} id 菜单目录id
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 单条菜单目录数据
 */
export const queryById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'system_menu/queryById', { id }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询所有菜单目录信息
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 所有菜单目录信息
 */
export const queryAll = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'system_menu/queryAll', { }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询所有菜单目录信息树形结构
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 所有菜单目录信息树形结构
 */
export const queryAllTree = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'system_menu/queryAllTree', { }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据菜单编码查询某一个子系统的菜单目录
 * @param {Number|String} menuCode 菜单编码，唯一索引
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 该子系统的菜单目录信息
 */
export const queryOneRootAllByCode = (menuCode, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'system_menu/queryOneRootAllByCode', { menuCode }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据菜单编码查询某一个子系统的菜单目录树形结构数据
 * @param {Number|String} menuCode 菜单编码，唯一索引
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 该子系统的菜单目录信息树形结构数据
 */
export const queryOneRootAllTreeByCode = (menuCode, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'system_menu/queryOneRootAllTreeByCode', { menuCode }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据父节点id获取子类节点列表
 * @param {Number|String} id 父节点id
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 子节点列表
 */
export const queryChildrenByParentId = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'system_menu/queryChildrenByParentId', { id }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询首层节点
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 首层节点
 */
export const queryRoots = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'system_menu/queryRoots', { }, {}, true,
    successCallback, null, fullCallback)
}

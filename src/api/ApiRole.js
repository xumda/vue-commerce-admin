import { fallbackRequestGet, fallbackRequestPost } from '@/utils/Request'

/**
 * 请求前缀，后面后台添加网关后需要前缀选择微服务
 */
const URL_PREFIX = 'middle-user/'

/**
 * 根据角色编号查询店铺可用角色信息
 * @param {Number|String} id 角色编号
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 角色数据
 */
export const queryStoreRoleById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'role/queryStoreRoleById', { id }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据角色编号查询官方后台系统可用角色信息
 * @param {Number|String} id 角色编号
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 角色数据
 */
export const queryAdminRoleById = (id, successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'role/queryAdminRoleById', { id }, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据角色编号查询店铺可用角色信息
 * @param {Array<Number|String>} ids 角色编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 角色数据列表
 */
export const queryStoreRolesByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/queryStoreRolesByIds', ids, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据角色编号查询官方后台系统可用角色信息
 * @param {Array<Number|String>} ids 角色编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 角色数据列表
 */
export const queryAdminRolesByIds = (ids, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/queryAdminRolesByIds', ids, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询当前登陆店铺可用所有角色信息
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 角色数据列表
 */
export const queryCurrentStoreAll = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'role/queryCurrentStoreAll', {}, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 查询后台管理员可用所有角色信息
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 角色数据列表
 */
export const queryCurrentAdminAll = (successCallback = null, fullCallback = null) => {
  return fallbackRequestGet(URL_PREFIX + 'role/queryCurrentAdminAll', {}, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 条件分页查询当前登陆店铺可用所有角色信息
 * @param {PageDTO<RoleQueryDTO>} pageDTO 条件分页数据
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 角色信息分页数据
 */
export const queryStoreRolePageWrapper = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/queryStoreRolePageWrapper', pageDTO, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 条件分页查询后台管理员可用所有角色信息
 * @param {PageDTO<RoleQueryDTO>} pageDTO 条件分页数据
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 角色信息分页数据
 */
export const queryAdminRolePageWrapper = (pageDTO, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/queryAdminRolePageWrapper', pageDTO, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 添加一条店铺角色信息数据
 * @param {Array<RoleInsertStoreDTO>} list 添加数据DTO列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 添加成功数据
 */
export const insertStoreRole = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/insertStoreRole', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 添加一条管理系统角色信息数据
 * @param {Array<RoleInsertAdminDTO>} list 添加数据DTO列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 添加成功数据
 */
export const insertAdminRole = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/insertAdminRole', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 修改一条店铺角色信息数据
 * @param {Array<RoleChangeDTO>} list 修改数据DTO列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 修改成功数据
 */
export const changeStoreRole = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/changeStoreRole', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 修改一条管理系统角色信息数据
 * @param {Array<RoleChangeDTO>} list 修改数据DTO列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 修改成功数据
 */
export const changeAdminRole = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/changeAdminRole', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据角色编号移除店铺角色信息
 * @param {Array<Number|String>} roleIds 角色编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否移除成功
 */
export const removeStoreRole = (roleIds, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/removeStoreRole', roleIds, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据角色编号移除管理系统角色信息
 * @param {Array<Number|String>} roleIds 角色编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否移除成功
 */
export const removeAdminRole = (roleIds, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/removeAdminRole', roleIds, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 修改一条店铺角色信息数据，固化数据修改
 * @param {Array<RoleChangeDTO>} list 修改数据DTO列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 修改成功数据
 */
export const changeStoreSolidifyRole = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/changeStoreSolidifyRole', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 修改一条管理系统角色信息数据，固化数据修改
 * @param {Array<RoleChangeDTO>} list 修改数据DTO列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 修改成功数据
 */
export const changeAdminSolidifyRole = (list, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/changeAdminSolidifyRole', list, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据角色编号移除店铺角色信息，固化数据移除
 * @param {Array<Number|String>} roleIds 角色编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否移除成功
 */
export const removeStoreSolidifyRole = (roleIds, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/removeStoreSolidifyRole', roleIds, {}, true,
    successCallback, null, fullCallback)
}

/**
 * 根据角色编号移除管理系统角色信息，固化数据移除
 * @param {Array<Number|String>} roleIds 角色编号列表
 * @param successCallback 成功回调
 * @param fullCallback 全回调
 * @return {*} 是否移除成功
 */
export const removeAdminSolidifyRole = (roleIds, successCallback = null, fullCallback = null) => {
  return fallbackRequestPost(URL_PREFIX + 'role/removeAdminSolidifyRole', roleIds, {}, true,
    successCallback, null, fullCallback)
}

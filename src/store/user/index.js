import constants from '@/store/user/constants'

const user = {
  namespaced: true,
  state: {
    userInfo: {
      account: ''
    },
    car: {
      goods: []
    }
  },
  getters: {
    isLogin (state) {
      return state.userInfo && state.userInfo.account
    }
  },
  mutations: {
    [constants.MUTATIONS_SET_LOGIN_USER_INFO] (state, userInfo) {
      state.userInfo = userInfo
    },
    updateCarGoods (state, goods) {
      state.car.goods = goods
    }
  }
}

export default user

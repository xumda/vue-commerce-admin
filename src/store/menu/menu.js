const accessMenu = [
  {
    path: '/index',
    name: 'index',
    title: '首页',
    icon: 'el-icon-s-home'
  }, {
    path: '/resource',
    name: 'resource',
    title: '资源管理',
    icon: 'el-icon-s-operation',
    children: [{
      path: '/index/pageMenu',
      name: 'pageMenu',
      title: '菜单管理',
      icon: 'el-icon-s-operation'
    }, {
      path: '/index/pageRole',
      name: 'pageRole',
      title: '角色管理',
      icon: 'el-icon-s-operation'
    }, {
      path: '/index/pageSolidifyRole',
      name: 'pageSolidifyRole',
      title: '固化角色',
      icon: 'el-icon-s-operation'
    }, {
      path: '/index/pagePermission',
      name: 'pagePermission',
      title: '权限管理',
      icon: 'el-icon-s-operation'
    }]
  }, {
    path: '/page',
    name: 'page',
    title: '页面管理',
    icon: 'el-icon-s-operation',
    children: [{
      path: '/index/pageIndexCarousel',
      name: 'pageIndexCarousel',
      title: '轮播图',
      icon: 'el-icon-s-operation'
    }, {
      path: '/index/pageIndexFeatured',
      name: 'pageIndexFeatured',
      title: '特色推荐',
      icon: 'el-icon-s-operation'
    }]
  }, {
    path: '/store',
    name: 'store',
    title: '店铺管理',
    icon: 'el-icon-s-operation',
    children: [{
      path: '/index/storeCategory',
      name: 'storeCategory',
      title: '店铺分类',
      icon: 'el-icon-s-operation'
    }]
  }, {
    path: '/goods',
    name: 'goods',
    title: '商品管理',
    icon: 'el-icon-s-operation',
    children: [{
      path: '/index/goodCategory',
      name: 'goodCategory',
      title: '商品分类',
      icon: 'el-icon-s-operation'
    }, {
      path: '/index/goodsCheck',
      name: 'goodsCheck',
      title: '商品审核',
      icon: 'el-icon-s-operation'
    }]
  }
]

const menu = {
  namespaced: true,
  state: {
    accessMenu: accessMenu
  },
  getters: {},
  mutations: {
  },
  actions: {
  }
}

export default menu

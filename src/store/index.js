import Vue from 'vue'
import Vuex from 'vuex'

import user from './user/index'
import request from './request/index'
import menu from './menu/menu'

// 混入，将store注入到每一个子组件中
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    request,
    menu
  }
})

export default store

# e-commerce 后台  

[https://gitee.com/xumda/e-commerce.git](https://gitee.com/xumda/e-commerce.git)

# e-commerce-back 后台配置中心  

[https://gitee.com/xumda/e-commerce-config.git](https://gitee.com/xumda/e-commerce-config.git)

# vue-commerce 用户前端  

[https://gitee.com/xumda/vue-commerce.git](https://gitee.com/xumda/vue-commerce.git)

# vue-commerce-back 店鋪前端  

[https://gitee.com/xumda/vue-commerce-back.git](https://gitee.com/xumda/vue-commerce.git)

## Project setupgit 
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
